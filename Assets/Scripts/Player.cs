﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
	private	Rigidbody2D		body;
	private	Vector3			start;
	private	Collider2D	ground;
	private	Player[]		players;

	public	float			speed;
	public	bool			focus;
	public	float			jump;
	// Use this for initialization
	void Start ()
	{
		start = transform.position;
		body = GetComponent<Rigidbody2D> ();
		SetFocus (focus);
		players = GameObject.FindObjectsOfType<Player> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (focus) {
			if ((Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.LeftArrow))
				&& body.velocity.x > -speed * 2) {
				body.AddForce (Vector2.left * speed * 10f);
			} else if ((Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.RightArrow))
				&& body.velocity.x < speed * 2) {
				body.AddForce (Vector2.right * speed * 10f);
			}
			if (body.IsTouching (ground) && Input.GetKeyDown (KeyCode.Space)) {
				body.AddForce (Vector2.up * jump * 200f);
			}
		}
		if ((Input.GetKeyDown (KeyCode.R) || Input.GetKeyDown (KeyCode.Backspace)) || transform.position.y < 0f) {
			transform.position = start;
		}
	}

	void LateUpdate ()
	{
		if (focus) {
			foreach (Player player in players) {
				if (Input.GetKeyDown (KeyCode.Alpha1) && player.tag == "Red") {
					focus = false;
					player.SetFocus (true);
				} else if (Input.GetKeyDown (KeyCode.Alpha2) && player.tag == "Yellow") {
					focus = false;
					player.SetFocus (true);
				} else if (Input.GetKeyDown (KeyCode.Alpha3) && player.tag == "Blue") {
					focus = false;
					player.SetFocus (true);
				}
			}
		}
	}

	void OnCollisionEnter2D (Collision2D Coll)
	{
		foreach (ContactPoint2D contact in Coll.contacts) {
			if (contact.normal.Equals (Vector2.up)) {
				ground = contact.collider;
			}
		}
		if (ground && ground.name != name && (ground.tag == tag || ground.tag == "Platform")) {
			transform.SetParent (ground.transform);
		}
	}

	void OnCollisionExit2D (Collision2D Coll)
	{
		if (ground && Coll.Equals (ground)) {
			transform.parent = null;
		}
	}
	
	public void SetFocus (bool f)
	{
		focus = f;
		if (focus) {
			Camera.main.transform.SetParent (transform);
			Camera.main.transform.localPosition = Vector3.zero;
			body.constraints = RigidbodyConstraints2D.FreezeRotation;
		} else {
			body.constraints = RigidbodyConstraints2D.FreezeRotation ^ RigidbodyConstraints2D.FreezePositionX;
		}
	}

	public void ResetPostion ()
	{
		transform.position = start;
	}
}
