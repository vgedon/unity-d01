﻿using UnityEngine;
using System.Collections;

public class SlidingPlatform : MonoBehaviour
{

	private	float 			startTime;
	private	float 			journeyLength;
	private	Rigidbody2D		pBody;

	public	GameObject		platform;
	public	float			speed = 1f;
	public	bool			Looping = false;
	public	Transform		start;
	public	Transform		end;

	// Use this for initialization
	void Start ()
	{
		pBody = platform.GetComponent<Rigidbody2D> ();
		pBody.MovePosition (start.position);
		startTime = Time.time;
		journeyLength = Vector3.Distance (start.position, end.position);
	}

	// Update is called once per frame
	void Update ()
	{
		float distCovered = (Time.time - startTime);
		float fracJourney = distCovered / journeyLength;
		platform.transform.position = (Vector3.Lerp (start.position, end.position, fracJourney));
	}

	void LateUpdate ()
	{
		if (Vector3.Equals (platform.transform.position, end.position)) {
			var tmp = start;
			start = end;
			end = tmp;
			startTime = Time.time;
		}
	}
}
