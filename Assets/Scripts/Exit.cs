﻿using UnityEngine;
using System.Collections;

public class Exit : MonoBehaviour
{
	public GameObject	target;
	private bool		onExit = false;
	// Use this for initialization
	void Start ()
	{
		onExit = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
	}
	
	void OnTriggerEnter2D (Collider2D coll)
	{
		if (coll.Equals (target.GetComponent<Collider2D> ())) {
			target.GetComponent<Rigidbody2D> ().MovePosition (transform.position);
			target.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
			onExit = true;
			GameObject[] exits = GameObject.FindGameObjectsWithTag ("Exit");
			bool f = true;
			foreach (GameObject item in exits) {
				if (!item.GetComponent<Exit> ().IsOk ()) {
					f = false;
				}
			}
			if (f) {
				Debug.Log ("Level " + Application.loadedLevelName + " finished");
				if (Application.loadedLevel < Application.levelCount - 1)
					Application.LoadLevel (Application.loadedLevel + 1);
			}
		}
	}

	public bool IsOk ()
	{
		return (onExit);
	}
	
	void OnTriggerExit2D (Collider2D coll)
	{
		if (coll.Equals (target.GetComponent<Collider2D> ())) {
			onExit = false;
		}
	}

}
